from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.
@login_required
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "projects": project,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, pk):
    projects = get_object_or_404(Project, pk=pk)
    context = {
        "project_detail": projects,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
